﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public static class Players {

	public  enum  enumPlayers
	{
		Lisa = 0  /*,
		Stephen = 1,
		CrackDaddy = 2,
		Kevin = 3 */
	}

	public struct PlayerStats{
		public int dashforce;
		public int maxSpeed;
		public int health;
		public int lives;		
		public string PreFabPath;

	}

	public static  PlayerStats getInitialStats(enumPlayers enumPlayer){
		PlayerStats CurrentPlayer = new PlayerStats();
		if (enumPlayer == enumPlayers.Lisa) {
			CurrentPlayer.dashforce = 500;
			CurrentPlayer.maxSpeed = 250;
			CurrentPlayer.health = 10;
			CurrentPlayer.lives = 1;		
			CurrentPlayer.PreFabPath = "Prefabs/Player_Lisa";
		} else {
			Debug.Log ("Could not find player stats for this enum");
		}

		return CurrentPlayer;
	}


}
