﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;





public class Player  {


	#region Properties
	private Players.enumPlayers _PlayerType;
	public   Players.enumPlayers PlayerType
	{
		get
		{
			return  _PlayerType;
		}
	}


	private int _health;
	public int health
	{
		get{
			return _health;
		}
		set{
			_health = value;
			if (_health <= 0)
			{
				_Dead = true;
			}
		}
	}


	private int _lives;
	public int lives
	{
		get{
			return _lives;
		}
		set{
			_lives = value;
		}
	}

	private bool _Dead = false;
	public bool Dead 
	{
		get
		{
			// we can either raise an event instead of having a property or  handle this being true in the mono scripts.
			return  _Dead;
		}
	}


	private int _maxSpeed;
	public int maxSpeed
	{
		get{
			return _maxSpeed;
		}
		set{
			_maxSpeed = value;
		}
	}



	private int _DashForce;
	public int DashForce
	{
		get{
			return _DashForce;
		}
		set{
			_DashForce = value;

		}
	}

	public string PreFabPath;

	#endregion

	#region Constructors

	public Player(Players.enumPlayers newPlayerType)
	{
		this._PlayerType = newPlayerType;
		InitialisePlayer ();
	}

	#endregion



	#region Methods

	void InitialisePlayer()
	{
		// Get values for the type of player. well call something to get these values later.
		Players.PlayerStats x = Players.getInitialStats(_PlayerType);
		this.health = x.health;
		this.lives = x.lives;
		this.DashForce = x.dashforce;
		this.maxSpeed = x.maxSpeed;
		this.PreFabPath = x.PreFabPath;
	}


	#endregion

}
