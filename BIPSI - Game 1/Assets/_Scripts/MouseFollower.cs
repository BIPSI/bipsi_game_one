﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class MouseFollower : MonoBehaviour {

	private List<string> triggers = new List<string> ();
	private Animator animator;
	// Use this for initialization
	void Start () {
		animator = gameObject.GetComponent<Animator> ();

        //add all trigger parameters on the object animator to a list
        foreach(AnimatorControllerParameter param in animator.parameters)
        {
            if(param.type == AnimatorControllerParameterType.Trigger)
            {
                triggers.Add(param.name);
            }
        }
    }

	// Update is called once per frame
	void Update () {

		Vector3 pos = transform.position;
		Vector3 pz = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		//Debug.Log (string.Format("trans pos: {0}",transform.position));
		//Debug.Log (string.Format("pz: {0}",pz));

		// THis returns a range from -180 : 180 degrees where the 180/-180 points merge
		float Angle = Mathf.Atan2 (pz.y - pos.y, pz.x - pos.x) * (180/Mathf.PI);

		// 0 -> 360 degrees
		Angle += 180;

		//Debug.Log (string.Format("ANGLE: {0}",Angle));
		parseangle (Angle);		
	}

	private void parseangle(float angle) 
	{
        //set the trigger corresponding to the currently faced direction
        Mouse.direction myDir = Mouse.getDirectionFromAngle(angle);
        resetAndSetTrigger(myDir.ToString());
	}

	private void resetAndSetTrigger(string triggername)
	{
		animator.SetTrigger(triggername);

        /*
		foreach (string trigger in triggers) {
			if (trigger == triggername )
			{
				animator.SetTrigger (trigger);
			} else{
				animator.ResetTrigger (trigger);
			} */

    }


}

//test change