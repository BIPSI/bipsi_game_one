﻿using UnityEngine;
using System.Collections;

public class Mouse : MonoBehaviour
{
    public enum direction
    {
        down = 0,
        right = 2,
        left = 1,
        up = 3,
        neutral = 5
    }

    public static direction getDirectionFromAngle(float angle)
    {
        if (angle <= 135 && angle > 45)
        {
            return direction.down;
        }
        else if (angle <= 225 && angle > 135)
        {
            return direction.right;
        }
        else if (angle <= 315 && angle > 225)
        {
            return direction.up;
        }
        else if ((angle <= 360 && angle > 315) || (angle <= 45))
        {
            return direction.left;
        }
        else
        {
            Debug.Log(string.Format("Angle out of range {0}", angle));
            return direction.neutral;
        };
    }
}
