﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement_ex_poschange : MonoBehaviour {

	// Some thoughts on this style of controller
	// youll notice that you can get through the walls by trying hard enough (holding down the key). This is because since we are updating the position
	// we can update the position to be intersecting/ beyond the wall space.
	// We can get away with this style very easily as we dont need to calculate gravity or anything of the like. If we decay the .GetAxis faster (project setting) well also be alot
	// more responsive.

	// We can handle this by implementing the on collisionenter2D event and overwrite the position set but this could get annoying. Not sure if this event fires correctly though if you start the frame 
	// intersecting with the other collider or not.
	//Can/should probably check how other people handle this online.

	public float maxSpeed = 5f;

	// Use this for initialization
	void Start () {
	}

	// Update is called once per frame
	void Update () {
		// Input.GetAxis returns whether one of the axis buttons is being pressed. Named like this as it also handles joysticks
		// By pharasing it in terms of axis or button alises you allow players to rebind via the unity game interface

		// Need to multiply everything by time.deltatime. This is the gap between drawn frames and hence the gap between update calls
		// Fixed update on the other hand fires every fixed time period (every 0.02 s by default although this can be altered in the project settings
		Vector3 pos = transform.position;


		//Returns a float -1.0 : + 1.0
		pos += new Vector3( Input.GetAxis ("Horizontal")* maxSpeed  * Time.deltaTime, Input.GetAxis ("Vertical")  * maxSpeed * Time.deltaTime , 0);
		transform.position = pos;

		// This is just to lock the rotation of the game object, which we'll probably want.
		// Theres a freeze z rotation option on the Rigidbody2D component, left this here though to show how to access the rotation.
		Quaternion rot = Quaternion.Euler (0, 0, 0);
		transform.rotation = rot;

		// This writes to the console. Debugging here works analogously to sprocs (you cant step through it) 
		//so you need to write infotrmation to the console to figure out whats going on alot of the time.

		//This will write ALOT of information though so we wouldnt actually have this commented in unless we wanted to specifically check this.
		//Debug.Log (string.Format ("New Pos: [{0},{1}.{2}]", pos.x, pos.y, pos.z));

	}




}
