﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement_ex_Translate : MonoBehaviour {

	public float maxSpeed = 5f;
	private Rigidbody2D rb2d;
	// Use this for initialization
	void Start () {

		// start is the first thing called, its happens in the instantiantion of the gameobject so im fetching the rigidbody component here
		// you tend to apply physics on the rigidbody
		rb2d = this.gameObject.GetComponent<Rigidbody2D>();
	}

	// Update is called once per frame
	void Update () {

		Vector3 translation = new Vector3(Input.GetAxis ("Horizontal")* maxSpeed  * Time.deltaTime, Input.GetAxis ("Vertical")  * maxSpeed * Time.deltaTime , 0);
		transform.Translate (translation);

		//Debug.Log (string.Format ("New Pos: [{0},{1}.{2}]", velocity.x, velocity.y, velocity.z));

		// This is just to lock the rotation of the game object, which we'll probably want.
		// Theres a freeze z rotation option on the Rigidbody2D component, left this here though to show how to access the rotation.
		Quaternion rot = Quaternion.Euler (0, 0, 0);
		transform.rotation = rot;


	}
}
