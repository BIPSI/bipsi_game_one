﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement_ex_velocity : MonoBehaviour {
	
	private Rigidbody2D rb2d;
	private bool dashing = false;
	private float dashduration_base = 0.1f;
	private float dashduration = 0.1f;
	private float dash_cooldown = 0f;
	private float dash_cooldown_base = 1.5f;

	PlayerController PC;

	// Use this for initialization
	void Start () {

		// start is the first thing called, its happens in the instantiantion of the gameobject so im fetching the rigidbody component here
		// you tend to apply physics on the rigidbody
		rb2d = this.gameObject.GetComponent<Rigidbody2D>();
		PC = PlayerController.instance;			
	}
	
	// Update is called once per frame
	void Update () {		
		dash_cooldown -= Time.deltaTime;

		if (dash_cooldown <= 0) {
			if (Input.GetButtonDown ("Jump")) {				
				dash_cooldown = dash_cooldown_base;
				Vector3 pos = transform.position;
				Vector3 pz = Camera.main.ScreenToWorldPoint (Input.mousePosition);
				Vector2 delta = new Vector2 (pz.x - pos.x  , pz.y - pos.y);

				Vector2 Dash = (delta) / delta.magnitude;
				Dash *= PC.player.DashForce;
				Debug.Log (string.Format("Dashing with force {0}",Dash));
				rb2d.AddForce (Dash);


				dashing = true;
			}

		}

		if (dashing == true) {	
			dashduration -= Time.deltaTime;
			if (dashduration <= 0) {
				dashing = false;
				dashduration = dashduration_base;
			}
		} else {
			Vector3 velocity = new Vector3 (Input.GetAxis ("Horizontal") * PC.player.maxSpeed * Time.deltaTime, Input.GetAxis ("Vertical") * PC.player.maxSpeed * Time.deltaTime, 0);
			rb2d.velocity = velocity;		
		}

	}

	void OnCollisionEnter2D(Collision2D col) {
		Debug.Log (string.Format ("Collision between {0} and {1} ", col.gameObject.name, col.otherCollider.gameObject.name));
	}



}
