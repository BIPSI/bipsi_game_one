﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseFollower_spriterenderer : MonoBehaviour {

	public Sprite[] Sprites;
	private SpriteRenderer sr;
	// Use this for initialization
	void Start () {
		sr = gameObject.GetComponent<SpriteRenderer> ();
		// Doesnt seem to be a way to get possible triggers on an animator, ill need to look at this more. otherwise we need to make sure they stay consistent
	}

	// Update is called once per frame
	void Update () {

		Vector3 pos = transform.position;
		Vector3 pz = Camera.main.ScreenToWorldPoint (Input.mousePosition);

		//Debug.Log (string.Format("trans pos: {0}",transform.position));
		//Debug.Log (string.Format("pz: {0}",pz));

		// THis returns a range from -180 : 180 degrees where the 180/-180 points merge
		float Angle = Mathf.Atan2 (pz.y - pos.y, pz.x - pos.x) * (180/Mathf.PI);

		// 0 -> 360 degrees
		Angle += 180;

		//Debug.Log (string.Format("ANGLE: {0}",Angle));
		parseangle (Angle);		
	}

	//We Can look to add on to this and check the velocity to see if we want to play walking animations and the like
	// or we might leave this alone for now.

	private void parseangle(float angle) 
	{
        //set the sprite corresponding to the currently faced direction
        Mouse.direction myDir = Mouse.getDirectionFromAngle(angle);
        int intDir = Convert.ToInt32(myDir);
        sr.sprite = Sprites [intDir];
	}



}
