﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	public static PlayerController instance = null;
	public Player player;
	public GameObject Player_Go;

	void Awake()
	{
		if (instance == null) {
			instance = this;
		} else if (instance != this) {
			Destroy (gameObject);
		}		
		DontDestroyOnLoad (gameObject);
	}

	// Use this for initialization
	void Start () {
		player = new Player (Players.enumPlayers.Lisa);
		SpawnPlayer();
	}
	
	// Update is called once per frame
	void Update () {

		if (player.Dead){
			Debug.Log ("IM DEAD");

		}
		
	}

	void SpawnPlayer()
	{
		Player_Go = Instantiate((Resources.Load(player.PreFabPath) as GameObject), transform.position, Quaternion.identity);
		DontDestroyOnLoad (Player_Go);
	}
}
